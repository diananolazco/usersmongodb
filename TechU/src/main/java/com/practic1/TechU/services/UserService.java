package com.practic1.TechU.services;


import com.practic1.TechU.models.UserModel;
import com.practic1.TechU.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;


    public List<UserModel> findAll(){
        System.out.println("findAll");

        return this.userRepository.findAll(Sort.by(Sort.Direction.DESC,"age"));
 //       return this.userRepository.findAll();
    }

    public UserModel add(UserModel user){
        System.out.println("add Users");
        return this.userRepository.save(user);
    }

    public Optional <UserModel> findById(String id){
        System.out.println("Id the users");
        return  this.userRepository.findById(id);
    }
    public UserModel update(UserModel users){
        return this.userRepository.save(users);

    }
    public Boolean delete(String id){

        System.out.println("delete 3");
        boolean result = false;
        if(this.findById(id).isPresent() == true){

            this.userRepository.deleteById(id);
            result = true;
        }
        return  result;
    }

/*
    public List<UserModel> findByAgeDesc(int age) {
        return this.userRepository.findByAgeDesc(age);
    }

 */
}
