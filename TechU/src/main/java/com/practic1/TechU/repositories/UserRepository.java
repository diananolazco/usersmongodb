package com.practic1.TechU.repositories;

import com.practic1.TechU.models.UserModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends MongoRepository<UserModel, String> {
//    @Query("{$users.sort( { age :- 1})}")
//    public List<UserModel> findByAgeDesc(int age);

}
