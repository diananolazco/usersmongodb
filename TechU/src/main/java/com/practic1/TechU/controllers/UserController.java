package com.practic1.TechU.controllers;


import com.practic1.TechU.models.UserModel;
import com.practic1.TechU.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v3")
public class UserController {

    @Autowired
    UserService userService;

    @GetMapping("/users")
    public ResponseEntity<List<UserModel>> getUser(){
        System.out.println("getuser");

        return new ResponseEntity<>(
                this.userService.findAll(), HttpStatus.OK
        );
    }

    @GetMapping("/users/{id}")
        public ResponseEntity<Object> getUserId(@PathVariable String id){

        System.out.println("getUsersId");
        System.out.println("get id the users" + id);
        Optional<UserModel> result = this.userService.findById(id);

        return new ResponseEntity<>(
                result.isPresent() ? result.get(): "User not find ",
                result.isPresent() ? HttpStatus.OK :HttpStatus.NOT_FOUND

        );
    }
    @PostMapping("/users")
    public  ResponseEntity<UserModel> postUser( @RequestBody UserModel users){
        System.out.println("postUser");

        System.out.println("Id new" + users.getId());
        System.out.println("name new" + users.getName());
        System.out.println("age new" + users.getAge());

        return new ResponseEntity<>(
                this.userService.add(users),HttpStatus.CREATED
        );
    }
    @PutMapping ("/users/{id}")
    public ResponseEntity<UserModel> putUser(@PathVariable String id, @RequestBody UserModel users){
        System.out.println("puthUser");
        System.out.println("id"+ id);
        System.out.println("id update user" + users.getId());
        System.out.println("name update user" + users.getName());
        System.out.println("age update user" + users.getAge());

        Optional<UserModel> result = this.userService.findById(id);

        if (result.isPresent()){
            System.out.println("User update");
            this.userService.update(users);
        }

        return new ResponseEntity<>(
                users ,result.isPresent() ? HttpStatus.OK :HttpStatus.NOT_FOUND

        );

    }
    @DeleteMapping("/users/{id}")
    public ResponseEntity<String> deleteUser(@PathVariable String id){
        System.out.println("deleteUser");

      boolean result = this.userService.delete(id);


        return new ResponseEntity<>(
                result ? "user delete" : "user not delete ",
                result ? HttpStatus.OK : HttpStatus.NOT_FOUND

        );
    }
/*    @GetMapping("/users")
    public List<UserModel> findByAgeDesc(@RequestParam ("age") int age){
        return this.userService.findByAgeDesc(age);
    }
*/

}
