package com.practic1.TechU.controllers;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HiController {
    @RequestMapping()

    public String index(){
        return "Hi practic one";

    }

    @RequestMapping("/hi")
    public String hi(
            @RequestParam( value = "name" , defaultValue = "Tech U Mongo practice") String  name
    ){
        return String.format("Hi %s!" , name);
    }
}
