package com.practic1.TechU;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TechUApplication {

	public static void main(String[] args) {
		SpringApplication.run(TechUApplication.class, args);
	}

}

